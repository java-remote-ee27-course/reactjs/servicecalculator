# Service calculator demo

This project was created using ReactJS.
A simple calculator calculating the tip amount based on clients feedback.

- The tip is the percentage of price calculated by the average of two feedbacks.
- All components are re-usable.
- Conditional rendering of total price and Reset button.

![calculator1](./public/calc1.png)
![calculator2](./public/calc2.png)
![calculator4](./public/calc4.png)

## Created by:

Katlin Kalde
