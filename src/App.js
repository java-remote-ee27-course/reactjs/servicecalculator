import { useState } from "react";

const feedbackValues = [
  { tip: 0, feedback: "Dissatisfied (0%)" },
  { tip: 0.05, feedback: "It was ok (5%)" },
  { tip: 0.1, feedback: "It was good (10%)" },
  { tip: 0.2, feedback: "It was excellent (20%)" },
];

function App() {
  const [price, setPrice] = useState(0);
  const [myTip, setMyTip] = useState(feedbackValues[0].tip);
  const [friendTip, setFriendTip] = useState(feedbackValues[0].tip);
  const totalTip =
    Math.round((((myTip + friendTip) / 2) * price + Number.EPSILON) * 100) /
    100;
  const totalPrice =
    Math.round(((price + Number.EPSILON) * 100) / 100) + Number(totalTip);

  function resetAll() {
    setPrice(0);
    setMyTip(feedbackValues[0].tip);
    setFriendTip(feedbackValues[0].tip);
  }

  return (
    <div className="App">
      <Header />

      <BillPrice price={price} onSetPrice={setPrice}>
        How much was the bill?
      </BillPrice>

      <ServiceFeedback
        tip={myTip}
        onSetTip={setMyTip}
        feedback={feedbackValues}
      >
        How did you like the service?
      </ServiceFeedback>

      <ServiceFeedback
        tip={friendTip}
        onSetTip={setFriendTip}
        feedback={feedbackValues}
      >
        How did your friend like the service?
      </ServiceFeedback>

      {(price > 0 || myTip > 0 || friendTip > 0) && (
        <>
          <TotalPrice>
            You pay: {totalPrice} EUR ({price} EUR + {totalTip} EUR tip)
          </TotalPrice>

          <Reset resetAll={resetAll}>Reset</Reset>
        </>
      )}
    </div>
  );
}

function Header() {
  return (
    <header>
      <h1>Service calculator</h1>
    </header>
  );
}

function BillPrice({ price, onSetPrice, children }) {
  return (
    <div className="bill">
      <div className="bill-text">{children}</div>
      <input
        type="number"
        value={price}
        onChange={(e) => onSetPrice(Number(e.target.value))}
        className="bill-input"
      />
    </div>
  );
}

function ServiceFeedback({ tip, onSetTip, feedback, children }) {
  return (
    <div className="service">
      <div className="service-text">{children} </div>
      <select
        value={tip}
        onChange={(e) => onSetTip(Number(e.target.value))}
        className="service-select"
      >
        {Array.from(feedback).map((el) => (
          <option value={Object.values(el)[0]} key={Object.values(el)[0]}>
            {el.feedback}
          </option>
        ))}
      </select>
    </div>
  );
}

function TotalPrice({ children }) {
  return <div class="total">{children}</div>;
}

function Reset({ resetAll, children }) {
  return (
    <div className="reset">
      <button className="btn" onClick={resetAll}>
        {children}
      </button>
    </div>
  );
}
export default App;
